# Development Commands

Commonly used commands, here for use with the [Runme](https://marketplace.visualstudio.com/items?itemName=stateful.runme) plugin for VSCode.

## Clean and Fetch Packages

Fetch the latest packages

```sh { closeTerminalOnSuccess=false }
dart pub get
```

## Code Quality (Static) Checks

Lint and other static analysis checks

Dart Analyzer

```sh { closeTerminalOnSuccess=false }
dart analyze lib
```

Dart Fix

```sh { closeTerminalOnSuccess=false }
dart fix --dry-run
```

```sh { closeTerminalOnSuccess=false }
dart fix --apply
```

## Code Quality Tests

Unit tests

```sh { closeTerminalOnSuccess=false }
dart test
```

```sh { closeTerminalOnSuccess=false }
dart test --coverage=coverage
```

## Documentation

Generate documentation

```sh { closeTerminalOnSuccess=false }
dart doc
```

## Dependency Management

Show package dependency tree

```sh { closeTerminalOnSuccess=false }
dart pub deps --style=compact
```

```sh { closeTerminalOnSuccess=false }
dart pub deps
```

Show outdated packages

```sh { closeTerminalOnSuccess=false }
dart pub outdated
```

Upgrade packages; *avoiding* major version changes, and breaking changes (semver)

```sh { closeTerminalOnSuccess=false }
dart pub upgrade
```

Upgrade packages, *including* major version changes, and breaking changes (semver)

```sh { closeTerminalOnSuccess=false }
dart pub upgrade --major-versions
```

## Build and Release

```sh { closeTerminalOnSuccess=false }
dart pub publish --dry-run
```

```sh { closeTerminalOnSuccess=false }
dart pub publish
```
