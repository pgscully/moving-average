# MovingAverage

A library for the calculation of moving averages.

[![Codemagic build status](https://api.codemagic.io/apps/5de78bab3b536a000ed464e5/dart-package-test/status_badge.svg)](https://codemagic.io/apps/5de78bab3b536a000ed464e5/dart-package-test/latest_build)

## Usage

A simple usage example:

```dart
import 'package:moving_average/moving_average.dart';

main() {
  final values = [1, 2, 3, 4, 5];
  print("Values = $values");
  // Values = [1, 2, 3, 4, 5]

  final simpleMovingAverage = MovingAverage<num>(
    averageType: AverageType.simple,
    windowSize: 3,
    partialStart: true,
    getValue: (num n) => n,
    add: (List<num> data, num value) => value,
  );
  final movingAverage3 = simpleMovingAverage(values);
  print("Moving Average, size 3, partial = $movingAverage3");
  // Moving Average, size 3, partial = [1.0, 1.5, 2.0, 3.0, 4.0]

  final weightedMovingAverage = MovingAverage<num>(
    averageType: AverageType.weighted,
    windowSize: 3,
    getValue: (num n) => n,
    add: (List<num> data, num value) => value,
  );
  final weightedAverage3 = weightedMovingAverage(values);
  print("Weighted Moving Average, size 3 = $weightedAverage3");
  // Weighted Moving Average, size 3 = [2.3333333333333335, 3.3333333333333335, 4.333333333333333]
}
```

## Features and bugs

Please file feature requests and bugs at the [issue tracker](https://gitlab.com/pgscully/moving-average/issues).
