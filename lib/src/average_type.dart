/// Spell out the types of moving average calculations available in this
/// package.
enum AverageType {
  /// A simple moving average, where each element in the window is weighted
  /// equally.
  simple,

  /// A weighted moving average, where more recent elements in the window are
  /// more heavily weighted.
  weighted,

  /// An exponential moving average, where the current data point, and the
  /// previous moving average are combined to calculate the new moving average.
  exponential,
}
