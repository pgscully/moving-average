/*
 * Copyright (c) 2019. Peter Scully
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import 'dart:math';
import 'package:moving_average/src/average_type.dart';

// TODO(pgs): What about taking a List<T1> and returning a T2?
// e.g. List<WeighIn> => DataPoint

// Ignore format lint as it includes a code block.
// ignore: format-comment
/// A class to calculate simple, weighted or exponential moving averages of
/// lists of objects.
///
/// [averageType] is the type of calculation to use.
///
/// [windowSize] is the window size used when calculating simple and weighted
/// moving averages.
///
/// [partialStart] is used with simple or weighted moving averages to control
/// whether or not to include calculations on incomplete windows at the start of
/// the data.
///
/// [factor] is the value used when calculating exponential moving averages.  It
/// should be constrained by 0.0 <= factor <= 1.0.
///
/// [getValue] is a function to take a value T and return a number, e.g. the y
/// coordinate of a point.
///
/// [add] is a function to take a list of inputs, and an average value, and
/// create a new data point from the data points and the new value.  Examples
/// include using the middle x value of a set of points for the x value of the
/// resulting point, and the new average for the y value.
///
/// For calculations on lists of numbers, not objects, use the following for
/// getValue and add:
///
/// ```dart
/// getValue: (num n) => n,
/// add: (List<num> data, num value) => value,
/// ````
class MovingAverage<T> {
  /// Instantiate the class with the given parameters
  ///
  /// If [averageType] is [AverageType.simple] or [AverageType.weighted] then
  /// [factor] is ignored.
  ///
  /// If [averageType] is [AverageType.exponential] then [windowSize] and
  /// [partialStart] are ignored.
  MovingAverage({
    this.averageType = AverageType.simple,
    this.windowSize = 1,
    this.partialStart = false,
    this.factor = 1.0,
    required this.getValue,
    required this.add,
  });

  /// The type of calculation to use.
  final AverageType averageType;

  /// Used with simple or weighted moving averages to control whether or not to
  /// include calculations on incomplete windows at the start of the data.
  final bool partialStart;

  /// The value used when calculating exponential moving averages.  It should
  /// be constrained by 0.0 <= factor <= 1.0.
  final double factor;

  /// The window size used when calculating simple and weighted moving averages.
  final int windowSize;

  /// The function to take a value T and return a number, e.g. the y coordinate of a point.
  final num Function(T) getValue;

  /// A function to take a list of inputs, and an average value, and create a
  /// new data point from the data points and the new value.  Examples include
  /// using the middle x value of a set of points for the x value of the
  /// resulting point, and the new average for the y value.
  final T Function(List<T> data, num newValue) add;

  // TODO(pgs): Second constructor for Exponential Moving Average
  // TODO(pgs): Consider 2 factories that instantiates separate classes for
  // simple/weighted, and exponential?
  // TODO(pgs): Consider simplified constructors for List<num> => num

  /// Invoke the configured MovingAverage calculator on data.
  List<T> call(List<T> data) {
    final result = <T>[];
    num Function(List<num>) averageFunc;

    switch (averageType) {
      case AverageType.simple:
      case AverageType.weighted:
        averageFunc =
            (averageType == AverageType.simple) ? _mean : _weightedMean;
        final subLists = _window(data, windowSize, partialStart: partialStart);
        for (final l in subLists) {
          final values = l.map(getValue).toList();
          final average = averageFunc(values);
          final temp = add(l, average);
          result.add(temp);
        }

      case AverageType.exponential:
        result.addAll(_exponential(data, factor));
    }

    return result;
  }

  /// Calculate the sum of the first n integers.
  //
  // Uses Gauss' formula for the sum of the first n integers.
  //
  // Ignore a named constant for 2 because it's trivial.
  // ignore: no-magic-number
  int _sumToN(int n) => n * (n + 1) ~/ 2;

  /// Return a list of sublists of data of size size.
  ///
  /// If partialStart is true then sublists of size 1,2,...n at the start of
  /// data will also be included.
  Iterable<List<T>> _window(
    List<T> data,
    int size, {
    bool partialStart = false,
  }) sync* {
    var start = 0;
    var end = 0;
    if (partialStart) {
      end = 1;
      start = end - size;
    } else {
      start = 0;
      end = size;
    }
    while (end <= data.length) {
      yield data.sublist(max(start, 0), end);
      start++;
      end++;
    }

    return;
  }

  /// Calculate the exponential moving average of data, with a weight factor of
  /// factor.
  List<T> _exponential(List<T> data, double factor) {
    final result = <T>[];
    if (data.isEmpty) {
      return result;
    }

    var prev = getValue(data.first);
    for (int index = 0; index < data.length; index++) {
      prev = prev * (1.0 - factor) + getValue(data[index]) * factor;
      result.add(add([data[index]], prev));
    }

    return result;
  }

  /// Calculate the mean (average) of data.
  num _mean(List<num> data) =>
      // Keep the type on the closure parameter to make it clear what the function is doing.
      // ignore: avoid_types_on_closure_parameters
      data.fold(0, (num a, num b) => a + b) / data.length;

  /// Calculate the weighted mean of data.
  num _weightedMean(List<num> data) {
    num sum = 0;
    for (int index = 0; index < data.length; index++) {
      sum += data[index] * (index + 1);
    }

    return sum / _sumToN(data.length);
  }
}
