/*
 * Copyright (c) 2019. Peter Scully
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// This comment includes code blocks that appear in pub.dev, so keep its
// formatting.
// ignore: format-comment
/// A class to calculate simple, weighted or exponential moving averages of
/// lists of objects or numbers.
///
/// A simple moving average is the mean:
/// mean(1, 2, 3) = (1 + 2 + 3) / 3
///               = 2.0
///
/// A weighted moving average is a weighted mean:
/// weighted(2, 1, 4) = (1*2 + 2*1 + 3*4) / (1 + 2 + 3)
///                   = (2 + 2 + 12) / 6
///                   = 2.66666
///
/// Calculate a simple moving average on a list of numbers with:
/// ```dart
/// final simpleMovingAverage = MovingAverage<num>(
///     averageType: AverageType.simple,
///     windowSize: 3,
///     getValue: (num n) => n,
///     add: (List<num> data, num value) => value,
///   );
/// final result = simpleMovingAverage(input_data);
/// ```
///
/// Calculate a weighted moving average for a list of Points with:
/// ```dart
/// final weightedMovingAverage = MovingAverage<Point>(
///     averageType: AverageType.weighted,
///     windowSize: 2,
///     getValue: (Point p) => p.y,
///     add: (List<Point> data, num value) => Point(data.last.x, value),
///   );
/// final result = weightedMovingAverage(input_data);
/// ```
///
/// Calculate an exponential moving average for a list of Points with:
/// ```dart
/// final exponentialMovingAverage = MovingAverage<Point>(
///     averageType: AverageType.exponential,
///     factor: 0.1,
///     getValue: (Point p) => p.y,
///     add: (List<Point> data, num value) => Point(data.last.x, value),
///   );
/// final result = exponentialMovingAverage(input_data);
/// ```
library moving_average;

export 'src/average_type.dart';
export 'src/moving_average.dart';
