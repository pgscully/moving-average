// ignore_for_file: prefer-correct-identifier-length
class Point {
  Point(this.x, this.y);

  final double x;
  final double y;

  @override
  String toString() => '(x = $x, y = $y)';
}
