// ignore_for_file: avoid_print

import 'package:moving_average/moving_average.dart';

void main() {
  final values = [1, 2, 3, 4, 5];
  print('Values = $values');
  // Values = [1, 2, 3, 4, 5]

  final simpleMovingAverage = MovingAverage<num>(
    averageType: AverageType.simple,
    windowSize: 3,
    partialStart: true,
    getValue: (n) => n,
    add: (data, value) => value,
  );
  final movingAverage3 = simpleMovingAverage(values);
  print('Moving Average, size 3, partial = $movingAverage3');
  // Moving Average, size 3, partial = [1.0, 1.5, 2.0, 3.0, 4.0]

  final weightedMovingAverage = MovingAverage<num>(
    averageType: AverageType.weighted,
    windowSize: 3,
    getValue: (n) => n,
    add: (data, value) => value,
  );
  final weightedAverage3 = weightedMovingAverage(values);
  print('Weighted Moving Average, size 3 = $weightedAverage3');
  // Weighted Moving Average, size 3 = [2.3333333333333335, 3.3333333333333335, 4.333333333333333]
}
