/*
 * Copyright (c) 2019. Peter Scully
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// ignore_for_file: avoid_print

import 'package:moving_average/moving_average.dart';
import 'point.dart';

// This example is as long as it needs to be
// ignore: long-method
void main() {
  print('On Numbers:');

  final numbers = [1, 2, 3, 4, 5];
  print('Values = $numbers');

  print('\nSize = 2, no partials');
  final nsma2 = MovingAverage<num>(
    averageType: AverageType.simple,
    windowSize: 2,
    getValue: (n) => n,
    add: (data, value) => value,
  );
  final numMovingAverage2 = nsma2(numbers);
  print('Simple Moving Average, size 2 = $numMovingAverage2');

  print('\nOn Objects:');

  final points = <Point>[
    Point(1, 1),
    Point(2, 2),
    Point(3, 3),
    Point(4, 4),
    Point(5, 5),
  ];
  print('Points = $points');

  print('\nSize = 2, no partials');

  final sma2 = MovingAverage<Point>(
    averageType: AverageType.simple,
    windowSize: 2,
    getValue: (p) => p.y,
    add: (data, value) => Point(data.last.x, value as double),
  );
  final movingAverage2 = sma2(points);
  print('Simple Moving Average, size 2 = $movingAverage2');

  final wma2 = MovingAverage<Point>(
    averageType: AverageType.weighted,
    windowSize: 2,
    getValue: (p) => p.y,
    add: (data, value) => Point(data.last.x, value as double),
  );
  final weightedAverage2 = wma2(points);
  print('Weighted Moving Average, size 2 = $weightedAverage2');

  print('\nSize = 3, partialStart = true');

  final sma3p = MovingAverage<Point>(
    averageType: AverageType.simple,
    windowSize: 3,
    partialStart: true,
    getValue: (p) => p.y,
    add: (data, value) => Point(data.last.x, value as double),
  );
  final movingAverage3 = sma3p(points);
  print('Simple Moving Average, size 3 = $movingAverage3');

  final wma3p = MovingAverage<Point>(
    averageType: AverageType.weighted,
    windowSize: 3,
    partialStart: true,
    getValue: (p) => p.y,
    add: (data, value) => Point(data.last.x, value as double),
  );
  final weightedAverage3 = wma3p(points);
  print('Weighted Moving Average, size 3 = $weightedAverage3');

  print('\nExponential Moving Average, 0.1');

  final ema = MovingAverage<Point>(
    averageType: AverageType.exponential,
    factor: 0.1,
    getValue: (p) => p.y,
    add: (data, value) => Point(data.last.x, value as double),
  );
  final exponential = ema(points);
  print('Exponential Moving Average, 0.1 = $exponential');
}
