# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.3.1 - 2023-01-27

- Update dependencies
- Use dart_code_metrics for static analysis
- Update lint rules
- Update to pass analysis

## 0.3.0 - 2021-02-17

- Remove nullsafety from version number

## 0.3.0-nullsafety.0 - 2020-11-23

- First version with null safety support

## 0.2.1 - 2020-08-11

### Added

- Add dartdoc to library classes and functions

## 0.2.0 - 2020-08-11

### Added

- Make a class that can be instantiated with required parameters, and called as necessary

### Changed

- Switch to lint package for static analysis

## 0.1.0-alpha4

- Add exponential moving averages

## 0.1.0-alpha3

- Use analysis_options from flutter repo
- Update code to pass analysis

## 0.1.0-alpha2

- Update package description

## 0.1.0-alpha

- Initial version, created by Peter Scully <peter@peterscully.name>
