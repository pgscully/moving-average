/*
 * Copyright (c) 2019. Peter Scully
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import 'package:moving_average/moving_average.dart';
import 'package:test/test.dart';
import 'point2d.dart';

// Long tests are OK
// ignore: long-method
void main() {
  group('Tests on numbers', () {
    final sma2 = MovingAverage<num>(
      averageType: AverageType.simple,
      windowSize: 2,
      getValue: (v) => v,
      add: (data, value) => value,
    );
    test('SMA of [1..5]', () {
      final input = <num>[1, 2, 3, 4, 5];
      final expected = <num>[1.5, 2.5, 3.5, 4.5];
      final result = sma2(input);
      expect(result, expected);
    });
  });

  group('Simple Moving Average (SMA) tests', () {
    final sma2 = MovingAverage<Point2D>(
      averageType: AverageType.simple,
      windowSize: 2,
      getValue: (v) => v.y,
      add: (data, value) => Point2D(data.last.x, value.toDouble()),
    );

    final sma2p = MovingAverage<Point2D>(
      averageType: AverageType.simple,
      windowSize: 2,
      partialStart: true,
      getValue: (v) => v.y,
      add: (data, value) => Point2D(data.last.x, value.toDouble()),
    );

    test('SMA of [], size 2, no partial is []', () {
      final input = <Point2D>[];
      final expected = <Point2D>[];
      final result = sma2(input);
      expect(result, expected);
    });

    test('SMA of [(1,1)], size 2, no partial', () {
      final input = <Point2D>[Point2D(1, 1)];
      final expected = <Point2D>[];
      final result = sma2(input);
      expect(result, expected);
    });

    test('SMA of [(1,1)], size 2, partialStart', () {
      final input = <Point2D>[Point2D(1, 1)];
      final expected = <Point2D>[Point2D(1, 1)];
      final result = sma2p(input);
      expect(result, expected);
    });

    test('SMA of [(1, 1)..(5,5)], size 2, no partial', () {
      final input = <Point2D>[
        Point2D(1.0, 1.0),
        Point2D(2.0, 2.0),
        Point2D(3.0, 3.0),
        Point2D(4.0, 4.0),
        Point2D(5.0, 5.0),
      ];
      final expected = <Point2D>[
        Point2D(2.0, 1.5),
        Point2D(3.0, 2.5),
        Point2D(4.0, 3.5),
        Point2D(5.0, 4.5),
      ];
      final result = sma2(input);
      expect(result, expected);
    });

    test('SMA of [(1, 1)..(5,5)], size 2, partialStart', () {
      final input = <Point2D>[
        Point2D(1.0, 1.0),
        Point2D(2.0, 2.0),
        Point2D(3.0, 3.0),
        Point2D(4.0, 4.0),
        Point2D(5.0, 5.0),
      ];
      final expected = <Point2D>[
        Point2D(1.0, 1.0),
        Point2D(2.0, 1.5),
        Point2D(3.0, 2.5),
        Point2D(4.0, 3.5),
        Point2D(5.0, 4.5),
      ];
      final result = sma2p(input);
      expect(result, expected);
    });
  });

  group('Weighted Moving Average (WMA) tests', () {
    final wma2 = MovingAverage<Point2D>(
      averageType: AverageType.weighted,
      windowSize: 2,
      getValue: (v) => v.y,
      add: (data, value) => Point2D(data.last.x, value.toDouble()),
    );

    final wma2p = MovingAverage<Point2D>(
      averageType: AverageType.weighted,
      windowSize: 2,
      partialStart: true,
      getValue: (v) => v.y,
      add: (data, value) => Point2D(data.last.x, value.toDouble()),
    );

    test('WMA of [], size 2, no partial is []', () {
      final input = <Point2D>[];
      final expected = <Point2D>[];
      final result = wma2(input);
      expect(result, expected);
    });

    test('WMA of [(1, 1)], size 2, no partial', () {
      final input = <Point2D>[Point2D(1, 1)];
      final expected = <Point2D>[];
      final result = wma2(input);
      expect(result, expected);
    });

    test('WMA of [(1, 1)], size 2, partialStart', () {
      final input = <Point2D>[Point2D(1, 1)];
      final expected = <Point2D>[Point2D(1, 1)];
      final result = wma2p(input);
      expect(result, expected);
    });

    test('WMA of [(1, 1)..(5,5)], size 2, no partial', () {
      final input = <Point2D>[
        Point2D(1, 1),
        Point2D(2, 2),
        Point2D(3, 3),
        Point2D(4, 4),
        Point2D(5, 5),
      ];
      final expected = <Point2D>[
        Point2D(2, 1.6666666666666667),
        Point2D(3, 2.6666666666666665),
        Point2D(4, 3.6666666666666665),
        Point2D(5, 4.666666666666667),
      ];
      final result = wma2(input);
      expect(result, expected);
    });

    test('WMA of [(1, 1)..(5,5)], size 2, partialStart', () {
      final input = <Point2D>[
        Point2D(1, 1),
        Point2D(2, 2),
        Point2D(3, 3),
        Point2D(4, 4),
        Point2D(5, 5),
      ];
      final expected = <Point2D>[
        Point2D(1, 1.0),
        Point2D(2, 1.6666666666666667),
        Point2D(3, 2.6666666666666665),
        Point2D(4, 3.6666666666666665),
        Point2D(5, 4.666666666666667),
      ];
      final result = wma2p(input);
      expect(result, expected);
    });
  });

  group('Exponential Moving Average tests', () {
    final ema = MovingAverage<num>(
      averageType: AverageType.exponential,
      factor: 0.1,
      getValue: (n) => n,
      add: (data, value) => value,
      // 0.1
    );

    test('Exponential Moving Average of []', () {
      final input = <num>[];
      final expected = <num>[];
      final result = ema(input);
      expect(result, expected);
    });

    test('Exponential Moving Average of [1]', () {
      final input = <num>[1];
      final expected = <num>[1];
      final result = ema(input);
      expect(result, expected);
    });

    test('Exponential Moving Average of [1..5]', () {
      final input = <num>[1, 2, 3, 4, 5];
      final expected = <num>[1, 1.1, 1.29, 1.561, 1.9049];
      final result = ema(input);
      expect(result, expected);
    });

    final p2dEma = MovingAverage<Point2D>(
      averageType: AverageType.exponential,
      factor: 0.1,
      getValue: (p) => p.y,
      add: (data, value) => Point2D(data.last.x, value.toDouble()),
      // 0.1
    );

    test('Exponential of [(1,1)]', () {
      final input = <Point2D>[Point2D(1, 1.0)];
      final expected = <Point2D>[Point2D(1, 1.0)];
      final result = p2dEma(input);
      expect(result, expected);
    });
    test('Exponential of [(1,1)..(5.5)]', () {
      final input = <Point2D>[
        Point2D(1, 1),
        Point2D(2, 2),
        Point2D(3, 3),
        Point2D(4, 4),
        Point2D(5, 5),
      ];
      final expected = <Point2D>[
        Point2D(1, 1.0),
        Point2D(2, 1.1),
        Point2D(3, 1.29),
        Point2D(4, 1.561),
        Point2D(5, 1.9049),
      ];
      final result = p2dEma(input);
      expect(result, expected);
    });
  });
}
